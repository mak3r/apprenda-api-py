#!/usr/bin/python

import argparse, apprenda, json, sys

parser = argparse.ArgumentParser(description='Install Authorization Portal components and Federation Service Provider on a running platform')
parser.add_argument(
    '-u', 
    '--user', 
    required=True, 
    help='This user must be an apprenda operator and have access to the developer portal through an apprenda tenant'
) 
parser.add_argument(
    '-p',
    '--password',
    required=True,
    help='The password for this administrative user'
)
parser.add_argument(
    '-t',
    '--tenant',
    required=True,
    help='The tenant that the user belongs to. The user must have access to the developer portal in this tenant'
)
parser.add_argument(
    '-c',
    '--cloud_url',
    required=True,
    help='The platform url which this setup will be deployed into'
)
parser.add_argument('--azp_archive',help='A apprenda app archive which will host the authorization portal site', default='./AzP-Core.zip')
parser.add_argument('--azp_app_alias',help='The application alias to use for the authorization portal site', default='authorization')
parser.add_argument('--eus_archive', help='The EUS plugin archive', default='./AzP-EUS.zip')
parser.add_argument('--auth_archive', help='The Authorization Extension archive', default='./AzP-Auth.zip')
parser.add_argument('--fsp_archive', help='The Federation Service Provider plugin archive', default='./FSP-LWA.zip')
parser.add_argument('--pause', help='Pause the script after installing the AzP Core. Resume by pressing any key. This may be necessary when the AzP needs to be populated with a valid user so we don\'t lock ourselves out.', action='store_true')

args = parser.parse_args()

soc_admin = args.user
soc_admin_pass = args.password
soc_admin_tenant = args.tenant
cloud_url = args.cloud_url

azp_archive = args.azp_archive
azp_app_alias = args.azp_app_alias 

eus_archive = args.eus_archive
auth_archive = args.auth_archive 
fsp_archive = args.fsp_archive 

#Get the auth token needed for remaining activities
print 'Getting the authentication token ..'
response = apprenda.authenticate_developer(soc_admin, soc_admin_pass, soc_admin_tenant, cloud_url)
print response
if response.status_code != 201:
    print 'Authentication failed. Unable to continue.\nError:\n' + response.text
    sys.exit(2)

j = json.loads(response.text)
token = j['apprendaSessionToken']
href = j['href']

#create upload and deploy AzP
print 'Setup the authorization core application AzP-core ..'
response = apprenda.developer.app_create(cloud_url, token, 'Authorization Portal', azp_app_alias)
print response
print 'Uploading Authorization Portal archive ..'
response = apprenda.developer.app_upload_archive(cloud_url, token, azp_app_alias, 'v1', azp_archive)
print response
print 'Publishing Authorization Portal archive ..'
response = apprenda.developer.app_publish(cloud_url, token, azp_app_alias, 'v1', 'Published')
print response
## TODO ##
#configure for soc admin user at a minimum
if args.pause:
    raw_input('Configure AzP, then press any key to continue.')

#upload and enable the auth extension
print 'Upload and enable the authorization extension AzP-Auth ..'
response = apprenda.soc.authn_upload_and_enable(cloud_url, token, auth_archive)
print response
#apprenda.soc.authn_disable(cloud_url, token)
auth_headers = 'Authentication.ExternalProvider.Headers'
print 'Checking that registry key exists for external auth provider headers' + auth_headers
response = apprenda.soc.get_from_registry(cloud_url, token, auth_headers)
print response
if response.status_code == 200:
    authn_headers = response.json()
    authn_headers['value'] = 'AzP-Service-Auth'
    print 'Setting external auth provider headers in the registry ..'
    response = apprenda.soc.update_registry(cloud_url, token, authn_headers)
    print response
else:
    print 'Failed to update registry key for Authentication.ExternalProvider.Headers'
    print 'This may require manual intervention'

#add the developer portal as an app everyone gets access to when they are onboarded
print 'Add the citadel extension required for AzP-Auth to the registry ..'
response = apprenda.soc.add_to_registry(
    cloud_url, 
    token,
    'Citadel.AdditionalApplicationOnboardList', 
    'developer,authorization', 
    'false')
print response

#add the extension point to the registry
print 'Add the citadel extension required for AzP-Auth to the registry ..'
response = apprenda.soc.add_to_registry(
    cloud_url, 
    token, 
    'CitadelExtServices ', 
    'authorization/CitadelExtension', 
    'false')
print response

#upload the eus plugin, test and enable it
print 'Upload and enable the external user store AzP-EUS ..'
#if apprenda.soc.eus_test(cloud_url, token, eus_archive) == 200:
if True:
    print '\tUploading ..'
    response = apprenda.soc.eus_upload(cloud_url, token, eus_archive)
    print response
    print '\tEnabling ..'
    response = apprenda.soc.eus_enable(cloud_url, token)
    print response

#upload the fsp plugin, test and enable it
print 'Upload and enable the federation service provider plugin FSP plugin ..'
#if apprenda.soc.fsp_test(cloud_url, token, fsp_archive) == 200:
if True:
    print '\tUploading ..'
    response = apprenda.soc.fsp_upload(cloud_url, token, fsp_archive)
    print response
    print '\tEnabling ..'
    response = apprenda.soc.fsp_enable(cloud_url, token)
    print response

#clear the session
print 'Clearing the session ..'
resonse = apprenda.delete_session(href)
print response
