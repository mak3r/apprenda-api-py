#!/bin/bash

curl -O https://s3.amazonaws.com/treadstone.public/apprenda-aws-automation/ansible/AzP-Core.zip
curl -O https://s3.amazonaws.com/treadstone.public/apprenda-aws-automation/ansible/AzP-Auth.zip
curl -O https://s3.amazonaws.com/treadstone.public/apprenda-aws-automation/ansible/AzP-EUS.zip
curl -O https://s3.amazonaws.com/treadstone.public/apprenda-aws-automation/ansible/FSP-LWA.zip

/c/users/mabrams/.platformio/python27/python ./setup_auth.py -u bxcr@apprenda.com -p password -t developer -c https://apps.apprenda.bxcr --pause
