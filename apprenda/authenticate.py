#!/usr/bin/python


import requests

def authenticate_soc(user, password, cloud_url):
    url = cloud_url + '/authentication/api/v1/sessions/soc'
    body = {'username': user, 'password': password}
    headers = {'Content-Type': 'application/json'}
    # alternatively/preferably we should set verify equal to the location of the ssl cert 
    r = requests.post(url, json=body, headers=headers, verify=False)

    return r

def authenticate_developer(user, password, tenant, cloud_url):
    url = cloud_url + '/authentication/api/v1/sessions/developer'
    body = {
        'username': user, 
        'password': password,
        'tenant': tenant,
    }
    headers = {'Content-Type': 'application/json'}
    # alternatively/preferably we should set verify equal to the location of the ssl cert 
    r = requests.post(url, json=body, headers=headers, verify=False)

    return r
