#!/usr/bin/python

import requests, json

eus_path = '/soc/api/v1/externaluserstore'
fsp_path = '/soc/api/v1/federationserviceplugin'
authn_path = '/soc/api/v1/externalauthentication'
registry_path = '/soc/api/v1/registry'
bsp_path = '/soc/api/v1/bootstrappolicies'

def eus_upload(cloud_url, session_token, archive_file):
    url = cloud_url + eus_path
    body = {'apprendaSessionToken': session_token}
    with open(archive_file, 'rb') as file:
        r = requests.put(url, data=file, json=body, verify=False)
    return r

def eus_test(cloud_url, session_token, archive_file):
    url = cloud_url + eus_path + '/test'
    headers = {'apprendaSessionToken': session_token}
    with open(archive_file, 'rb') as file:
        data = file.read()
        r = requests.post(url, data=data, headers=headers, verify=False)
    return r

def eus_enable(cloud_url, session_token):
    url = cloud_url + eus_path + '/enable'
    body = {'apprendaSessionToken': session_token}
    r = requests.post(url, json=body, verify=False)
    return r

def eus_disable(cloud_url, session_token):
    r = get_from_registry(cloud_url, session_token, 'Platform.ExternalUserStoreEnabled')
    entries = r.json()
    entries['value'] = 'False'
    r = update_registry(cloud_url, session_token, entries)
    return r

def fsp_upload(cloud_url, session_token, archive_file):
    url = cloud_url + fsp_path
    body = {'apprendaSessionToken': session_token}
    with open(archive_file, 'rb') as file:
        r = requests.put(url, data=file, json=body, verify=False)
    return r

def fsp_test(cloud_url, session_token, archive_file):
    url = cloud_url + fsp_path + '/test'
    headers = {'apprendaSessionToken': session_token}
    with open(archive_file, 'rb') as file:
        data = file.read()
        r = requests.post(url, data=data, headers=headers, verify=False)
    return r

def fsp_enable(cloud_url, session_token):
    url = cloud_url + fsp_path + '/enable'
    body = {'apprendaSessionToken': session_token}
    r = requests.post(url, json=body, verify=False)
    return r

def fsp_disable(cloud_url, session_token):
    url = cloud_url + bsp_path
    headers = {'apprendaSessionToken': session_token}
    r = requests.get(url, headers=headers, verify=False)
    policies = r.json()['items']
    for policy in policies:
        if policy['name'] == 'Federation Service Provider':
            url = cloud_url + bsp_path + '/' + policy['id']
            headers = {'apprendaSessionToken': session_token}
            r = requests.delete(url, headers=headers, verify=False)
            return r

def authn_upload_and_enable(cloud_url, session_token, archive_file):
    url = cloud_url + authn_path + '?action=update&error_code_override=200&accept_override=text/html'
    body = {'apprendaSessionToken': session_token}
    with open(archive_file, 'rb') as file:
        r = requests.post(url, data=file, json=body, verify=False)
    return r

def authn_disable(cloud_url, session_token):
    url = cloud_url + authn_path + '?action=clear'
    body = {'apprendaSessionToken': session_token}
    r = requests.post(url, json=body, verify=False)
    return r

def add_to_registry(cloud_url, session_token, key, value, encrypt):
    url = cloud_url + registry_path
    body = {
        'apprendaSessionToken': session_token,
        'name': key,
        'value': value,
        'isEncrypted': encrypt,
        'isReadonly': 'false'
    }
    r = requests.post(url, json=body, verify=False)
    return r    

def get_from_registry(cloud_url, session_token, key):
    url = cloud_url + registry_path + '/' + key
    body = {'apprendaSessionToken': session_token}
    r = requests.get(url, json=body, verify=False)
    return r

#The client is responsible for making sure the key already exists before 
# making the update. 
def update_registry(cloud_url, session_token, entries): 
    url = cloud_url + registry_path + '/' + entries['name']
    headers = {'apprendaSessionToken': session_token}
    r = requests.put(url, json=entries, headers=headers, verify=False)

    return r

def remove_from_registry(cloud_url, session_token, id):
    url = cloud_url + registry_path + '/' + id
    headers = {'apprendaSessionToken': session_token}
    r = requests.delete(url, headers=headers, verify=False)
    return r

