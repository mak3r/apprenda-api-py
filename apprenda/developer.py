#!/usr/bin/python

import requests, json, urllib3

#NO CERT VERIFICATION IN ANY CALLS RIGHT NOW 
# REDUCE THE NOISE
urllib3.disable_warnings()

apps_path = '/developer/api/v1/apps'
versions_path = '/developer/api/v1/versions'
token = 'apprendaSessionToken'

def app_create(cloud_url, session_token, name, alias):
    url = cloud_url + apps_path
    headers = {
        'apprendaSessionToken': session_token
    }
    body = {
        'name': name,
        'alias': alias
        }
    r = requests.post(url, headers=headers, json=body, verify=False)
    return r

def app_upload_archive(cloud_url, session_token, alias, version, archive_file):
    url = cloud_url + versions_path + '/' + alias + '/' + version + '?action=setArchive'
    # + token + '=' + session_token
    headers = {
        'Content-type': 'application/octet-stream',
        'apprendaSessionToken': session_token
    }
    with open(archive_file, 'rb') as file:
        data = file.read()
        r = requests.post(url, headers=headers, data=data, verify=False)
    return r

def app_publish(cloud_url, session_token, alias, version, stage):
    url = cloud_url + versions_path + '/' + alias + '/' + version + '?action=promote&async=true&stage=' + stage
    headers = {
        'apprendaSessionToken': session_token
    }
    r = requests.post(url, headers=headers, verify=False)
    return r

def app_delete(cloud_url, session_token, alias):
    url = cloud_url + apps_path + '/' + alias
    headers = {
        'apprendaSessionToken': session_token
    }
    r = requests.delete(url, headers=headers, verify=False)
    return r

def app_get(cloud_url, session_token, alias):
    url = cloud_url + apps_path + '/' + alias
    headers = {
        'apprendaSessionToken': session_token
    }
    r = requests.get(url, headers=headers, verify=False)
    return r

def app_exists(cloud_url, session_token, alias):    
    r = app_get(cloud_url, session_token, alias)
    j = json.loads(r.text)
    if r.status_code == 200 and j['alias'] == alias:
      return True
    else:
      return False
