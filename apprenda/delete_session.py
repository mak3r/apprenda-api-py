#!/usr/bin/python


import requests

def delete_session(url):
    # alternatively/preferably we should set verify equal to the location of the ssl cert 
    r = requests.delete(url, verify=False)
   
    return r
