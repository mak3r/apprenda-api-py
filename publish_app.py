#!/usr/bin/python

import argparse, apprenda, json, sys, getpass, logging
from apprenda import developer
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

parser = argparse.ArgumentParser(description='Prepare a developer for using other apprenda python modules')
parser.add_argument(
    '-u', 
    '--user', 
    required=True, 
    help='This user must be an apprenda developer and have access to the developer portal through an apprenda tenant'
) 
parser.add_argument(
    '-p',
    '--password',
    required=False,
    nargs='?',
    help='The password for this developer user. If no password value is given the user will be prompted.'
)
parser.add_argument(
    '-t',
    '--tenant',
    required=True,
    help='The tenant that the user belongs to. The user must have access to the developer portal in this tenant'
)
parser.add_argument(
    '-c',
    '--cloud_url',
    required=True,
    help='The platform url which this setup will be deployed into'
)

parser.add_argument('--app_alias',help='A apprenda app alias. The alias will also be used as the application name.', default='testapp')
parser.add_argument('--archive',help='A apprenda app archive which will be deployed and promoted to the published stage', default='./Archive.zip')
parser.add_argument('--delete', help='Delete the existing app of the same name if it exists.', action='store_true')
parser.add_argument('--stage', help='Default is to Publish the app. Apprenda app stages [Definition | Sandbox | Published]', default='Published')

args = parser.parse_args()
logging.debug( args)

platform_dev = args.user
platform_dev_pass = args.password
platform_dev_tenant = args.tenant
cloud_url = args.cloud_url

#Get the users password if they did not pass it as an argument
if platform_dev_pass == None:
    platform_dev_pass = getpass.getpass()


#Get the auth token needed for remaining activities
logging.info('Getting the authentication token ..')
response = apprenda.authenticate_developer(platform_dev, platform_dev_pass, platform_dev_tenant, cloud_url)
logging.debug(response)
if response.status_code != 201:
    logging.error('Authentication failed. Unable to continue.\nError:\n' + response.text)
    sys.exit(2)

j = json.loads(response.text)
token = j['apprendaSessionToken']
href = j['href']

app_alias = args.app_alias
app_name = args.app_alias
app_archive = args.archive
stage = args.stage

def create_app(cloud_url, token, app_name, app_alias, app_archive):
    #create, upload and deploy an app
    logging.info('Creating the base application definition', )
    response = apprenda.developer.app_create(cloud_url, token, app_name, app_alias)
    logging.debug(response)
    if stage != 'Definition':
        logging.info('Uploading application archive ..')
        response = apprenda.developer.app_upload_archive(cloud_url, token, app_alias, 'v1', app_archive)
        logging.debug(response)
        logging.info('Publishing the application ..')
        response = apprenda.developer.app_publish(cloud_url, token, app_alias, 'v1', stage)
        logging.debug(response)

app_exists = developer.app_exists(cloud_url, token, app_alias)
if app_exists:
    if args.delete:
        logging.info('Deleting existing app')
        developer.app_delete(cloud_url, token, app_alias)
        create_app(cloud_url, token, app_name, app_alias, app_archive)
    else:
        logging.info('DONT delete existing app. No further action will be taken. Use --delete to allow deletion.')
else:
    create_app(cloud_url, token, app_name, app_alias, app_archive)


#clear the session
logging.debug('Clearing the session ..')
resonse = apprenda.delete_session(href)
logging.debug(response)


