#!/usr/bin/python

import argparse, apprenda, json, sys

parser = argparse.ArgumentParser(description='Remove Authorization Portal components and Federation Service Provider froom a running platform')
parser.add_argument(
    '-p',
    '--password',
    required=True,
    help='The password for this administrative user'
)
parser.add_argument(
    '-t',
    '--tenant',
    required=True,
    help='The tenant that the user belongs to. The user must have access to the developer portal in this tenant'
)
parser.add_argument(
    '-c',
    '--cloud_url',
    required=True,
    help='The platform url which this setup will be deployed into'
)
empty_archive='./empty.zip'
parser.add_argument('--eus_archive', help='The EUS plugin archive', default=empty_archive)
parser.add_argument('--auth_archive', help='The Authorization Extension archive', default=empty_archive)
parser.add_argument('--fsp_archive', help='The Federation Service Provider plugin archive', default=empty_archive)

args = parser.parse_args()

soc_admin = 'admin@apprenda.com'
soc_admin_pass = args.password
soc_admin_tenant = args.tenant
cloud_url = args.cloud_url

eus_archive = args.eus_archive
auth_archive = args.auth_archive 
fsp_archive = args.fsp_archive 

#Get the auth token needed for remaining activities
print 'Getting the authentication token ..'
response = apprenda.authenticate_developer(soc_admin, soc_admin_pass, soc_admin_tenant, cloud_url)
print response
if response.status_code != 201:
    print 'Authentication failed. Unable to continue.\nError:\n' + response.text
    sys.exit(2)

j = json.loads(response.text)
token = j['apprendaSessionToken']
href = j['href']

#upload and disable the auth extension
print 'Upload empty archive and disable the authorization extension AzP-Auth ..'
response = apprenda.soc.authn_upload_and_enable(cloud_url, token, auth_archive)
print response
#apprenda.soc.authn_disable(cloud_url, token)
auth_headers = 'Authentication.ExternalProvider.Headers'
print 'Checking that registry key exists for external auth provider headers' + auth_headers
response = apprenda.soc.get_from_registry(cloud_url, token, auth_headers)
print response
if response.status_code == 200:
    authn_headers = response.json()
    authn_headers['value'] = ''
    print 'Removing external auth provider headers in the registry ..'
    response = apprenda.soc.update_registry(cloud_url, token, authn_headers)
    print response
else:
    print 'Failed to update registry key for Authentication.ExternalProvider.Headers'
    print 'This may require manual intervention'

#add the extension point to the registry
print 'Remove the citadel extension required for AzP-Auth to the registry ..'
response = apprenda.soc.remove_from_registry(
    cloud_url, 
    token, 
    'CitadelExtServices') 
print response

#upload the empty eus plugin 
print 'Upload empty archive and disable the external user store AzP-EUS ..'
#if apprenda.soc.eus_test(cloud_url, token, eus_archive) == 200:
if True:
    print '\tUploading ..'
    response = apprenda.soc.eus_upload(cloud_url, token, eus_archive)
    print response
    print '\tDisabling..'
    response = apprenda.soc.eus_disable(cloud_url, token)
    print response

#upload the empty fsp plugin
print 'Upload empty archive and disable the federation service provider plugin FSP plugin ..'
#if apprenda.soc.fsp_test(cloud_url, token, fsp_archive) == 200:
if True:
    print '\tUploading ..'
    response = apprenda.soc.fsp_upload(cloud_url, token, fsp_archive)
    print response
    print '\tDisabling..'
    response = apprenda.soc.fsp_disable(cloud_url, token)
    print response

print 'The platform configuration has been cleaned up'
print 'Now go and redeploy the authentication UI and service from the SOC'

#clear the session
print 'Clearing the session ..'
resonse = apprenda.delete_session(href)
print response
